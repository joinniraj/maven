package com.itn.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-10T10:35:12")
@StaticMetamodel(Seminar.class)
public class Seminar_ { 

    public static volatile SingularAttribute<Seminar, String> seminarbuilding;
    public static volatile SingularAttribute<Seminar, String> seminardate;
    public static volatile SingularAttribute<Seminar, String> seminarduration;
    public static volatile SingularAttribute<Seminar, String> seminarmaster;
    public static volatile SingularAttribute<Seminar, String> seminarname;
    public static volatile SingularAttribute<Seminar, Integer> sid;

}