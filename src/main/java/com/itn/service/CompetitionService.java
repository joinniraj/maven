/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.service;
import com.itn.dao.CompetitionDao;
import com.itn.entity.Competition;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rebel
 */
@Service
public class CompetitionService implements CompetitionServiceInterface {
    @Autowired
    CompetitionDao cdao;

    public CompetitionDao getCdao() {
        return cdao;
    }

    public void setCdao(CompetitionDao cdao) {
        this.cdao = cdao;
    }

    @Transactional
    @Override
    public void insert(Competition c) {
        cdao.insert (c);
    }

    @Override
    public List<Competition> display() {
         return cdao.display();
    }

    @Transactional  //begin of the transaction
    @Override
    public void delete(int cid) {
        cdao.delete(cid);
    }
    @Transactional
    @Override
    public Competition display_by_id(int cid) {
        return cdao.display_by_id(cid);
    }
    @Transactional
    @Override
    public void update(Competition c) {
        cdao.update(c);
    }
  
}
