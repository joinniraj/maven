/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.service;

import com.itn.dao.SeminarDao;
import com.itn.entity.Seminar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rebel
 */
@Service
public class SeminarService implements SeminarServiceInterface {
@Autowired
    SeminarDao sdao;

    public SeminarDao getSdao() {
        return sdao;
    }

    public void setSdao(SeminarDao sdao) {
        this.sdao = sdao;
    }
    
    
    @Transactional
    @Override
    public void insert(Seminar s) {
        sdao.insert (s);
    }

    @Override
    public List<Seminar> display() {
        return sdao.display();
    }
    @Transactional
    @Override
    public void delete(int sid) {
         sdao.delete(sid);
    }
    @Transactional
    @Override
    public Seminar display_by_id(int sid) {
        return sdao.display_by_id(sid);
    }
    @Transactional
    @Override
    public void update(Seminar s) {
        sdao.update(s);
    }
    
}
