/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.dao;

import com.itn.entity.Competition;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Transaction;

/**
 *
 * @author Rebel
 */
@Repository
public class CompetitionDao implements CompetitionDaoInterface {
 @Autowired
    
SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
 

    @Override
    public void insert(Competition c) {
        Session sess=sessionFactory.openSession();
        sess.save(c);
        sess.close();
    }

    @Override
    public List<Competition> display() {
        Session sess=sessionFactory.openSession();
    List<Competition> c=sess.createCriteria(Competition.class).list();
        return c;
    }

    @Override
    public void delete(int cid) {
        Session sess=sessionFactory.openSession();
        Transaction tx=sess.beginTransaction();
        Competition c=(Competition)sess.load(Competition.class, cid);       
        sess.delete(c);
        tx.commit();
    }

    @Override
    public Competition display_by_id(int cid) {
     Session sess=sessionFactory.openSession();
             Competition c=(Competition)sess.get(Competition.class, cid); 
             return c;

    }

    @Override
    public void update(Competition c) {
        Session sess=sessionFactory.openSession();
        Transaction tx=sess.beginTransaction();
        sess.update(c);
        tx.commit();
        
    }
    
}
