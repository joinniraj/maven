/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.dao;

import com.itn.entity.Competition;
import java.util.List;

/**
 *
 * @author Rebel
 */
public interface CompetitionDaoInterface {
    public void insert(Competition c);
    public List<Competition>display();
    public void delete(int cid);
    public Competition display_by_id(int cid);
    public void update(Competition c); 
    
}
