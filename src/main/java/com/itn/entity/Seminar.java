/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rebel
 */
@Entity
@Table(name = "seminar", catalog = "eventmanagement", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seminar.findAll", query = "SELECT s FROM Seminar s"),
    @NamedQuery(name = "Seminar.findBySid", query = "SELECT s FROM Seminar s WHERE s.sid = :sid"),
    @NamedQuery(name = "Seminar.findBySeminarname", query = "SELECT s FROM Seminar s WHERE s.seminarname = :seminarname"),
    @NamedQuery(name = "Seminar.findBySeminardate", query = "SELECT s FROM Seminar s WHERE s.seminardate = :seminardate"),
    @NamedQuery(name = "Seminar.findBySeminarduration", query = "SELECT s FROM Seminar s WHERE s.seminarduration = :seminarduration"),
    @NamedQuery(name = "Seminar.findBySeminarbuilding", query = "SELECT s FROM Seminar s WHERE s.seminarbuilding = :seminarbuilding"),
    @NamedQuery(name = "Seminar.findBySeminarmaster", query = "SELECT s FROM Seminar s WHERE s.seminarmaster = :seminarmaster")})
public class Seminar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sid")
    private Integer sid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 78)
    @Column(name = "seminarname")
    private String seminarname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 67)
    @Column(name = "seminardate")
    private String seminardate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "seminarduration")
    private String seminarduration;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "seminarbuilding")
    private String seminarbuilding;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "seminarmaster")
    private String seminarmaster;

    public Seminar() {
    }

    public Seminar(Integer sid) {
        this.sid = sid;
    }

    public Seminar(Integer sid, String seminarname, String seminardate, String seminarduration, String seminarbuilding, String seminarmaster) {
        this.sid = sid;
        this.seminarname = seminarname;
        this.seminardate = seminardate;
        this.seminarduration = seminarduration;
        this.seminarbuilding = seminarbuilding;
        this.seminarmaster = seminarmaster;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSeminarname() {
        return seminarname;
    }

    public void setSeminarname(String seminarname) {
        this.seminarname = seminarname;
    }

    public String getSeminardate() {
        return seminardate;
    }

    public void setSeminardate(String seminardate) {
        this.seminardate = seminardate;
    }

    public String getSeminarduration() {
        return seminarduration;
    }

    public void setSeminarduration(String seminarduration) {
        this.seminarduration = seminarduration;
    }

    public String getSeminarbuilding() {
        return seminarbuilding;
    }

    public void setSeminarbuilding(String seminarbuilding) {
        this.seminarbuilding = seminarbuilding;
    }

    public String getSeminarmaster() {
        return seminarmaster;
    }

    public void setSeminarmaster(String seminarmaster) {
        this.seminarmaster = seminarmaster;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sid != null ? sid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seminar)) {
            return false;
        }
        Seminar other = (Seminar) object;
        if ((this.sid == null && other.sid != null) || (this.sid != null && !this.sid.equals(other.sid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.entity.Seminar[ sid=" + sid + " ]";
    }
    
}
