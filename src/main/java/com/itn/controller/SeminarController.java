/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.controller;



import com.itn.entity.Seminar;
import com.itn.service.SeminarService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Rebel
 */
@Controller
public class SeminarController {
    @Autowired    
    SeminarService seminarService;

    public SeminarService getSeminarService() {
        return seminarService;
    }

    public void setSeminarService(SeminarService seminarService) {
        this.seminarService = seminarService;
    }
    
    @RequestMapping(value="/AddSeminar", method=RequestMethod.GET)
    public ModelAndView addSeminar(){
        return new ModelAndView("AddSeminar");
    }
    
    @RequestMapping(value="/SubmitAddSeminar", method=RequestMethod.POST)
    public ModelAndView submitAddSeminar(@ModelAttribute Seminar s){
            seminarService.insert(s);
    return new ModelAndView("AddSeminar");    
}
    @RequestMapping(value="/DisplaySeminar", method=RequestMethod.GET )
    public ModelAndView displaySeminar(){
        List<Seminar> s=seminarService.display();
        return new ModelAndView("DisplaySeminar","seminar",s);
}
    @RequestMapping(value="/DeleteSeminar", method=RequestMethod.GET )
     public ModelAndView deleteSem(@RequestParam("sid") int sid){
        seminarService.delete(sid);
        return new ModelAndView("redirect:DisplaySeminar");
     }
     @RequestMapping(value="/EditSeminar", method=RequestMethod.GET )
      public ModelAndView editSeminar(@RequestParam("sid") int sid){
       Seminar s=seminarService.display_by_id(sid);
       return new ModelAndView("EditSeminar","seminar",s);
      }
      @RequestMapping(value="/SubmitEditSeminar", method=RequestMethod.POST)
    public ModelAndView submitEditSeminar(@ModelAttribute Seminar s){
    seminarService.update(s);
    return new ModelAndView("redirect:DisplaySeminar");
    }
}
