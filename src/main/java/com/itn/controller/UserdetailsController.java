/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.controller;

import com.itn.entity.Userdetails;
import com.itn.service.UserdetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rebel
 */
@Controller
public class UserdetailsController {
      @Autowired
       UserdetailsService userdetailsService;

    public UserdetailsService getUserdetailsService() {
        return userdetailsService;
    }

    public void setuserdetailsService(UserdetailsService userdetailsService) {
        this.userdetailsService = userdetailsService;
    }
    
    @RequestMapping(value="/AddUser", method=RequestMethod.GET)
    public ModelAndView addUser(){
        return new ModelAndView("AddUser");
    }
    
        
    @RequestMapping(value="/login", method=RequestMethod.GET)
    public ModelAndView login(@ModelAttribute Userdetails u){
            
    return new ModelAndView("Login");
    
    }
    
}
