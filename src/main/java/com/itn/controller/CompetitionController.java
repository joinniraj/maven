/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.controller;
import com.itn.entity.Competition;
import com.itn.service.CompetitionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.ModelAttribute;
/**
 *
 * @author Rebel
 */
@Controller
public class CompetitionController {
    
    @Autowired    
    CompetitionService competitionService;

    public CompetitionService getCompetitionService() {
        return competitionService;
    }

    public void setCompetitionService(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }
    @RequestMapping(value="/Login", method=RequestMethod.GET)
    public ModelAndView login(@RequestParam(value="error",required=false) String error,@RequestParam(value="logout", required=false)String logout){
    ModelAndView model= new ModelAndView();
    if(error!=null)
    {
        model.addObject("error","Invalid username and password");
    }
    if(logout!=null){
    model.addObject("msg","log out");
    }
        return new ModelAndView("Login");
    }
    
    @RequestMapping(value="/AddCompetition", method=RequestMethod.GET)
    public ModelAndView addCompetition(){
        return new ModelAndView("AddCompetition");
    }
    
    @RequestMapping(value="/SubmitAddCompetition", method=RequestMethod.POST)
    public ModelAndView submitAddCompetition(@ModelAttribute Competition c){
            competitionService.insert(c);
    return new ModelAndView("AddCompetition");
    
}
    @RequestMapping(value="/DisplayCompetition", method=RequestMethod.GET )
    public ModelAndView displayCompetition(){
        List<Competition> c=competitionService.display();
        return new ModelAndView("DisplayCompetition","competition",c);
}
     @RequestMapping(value="/DeleteCompetition", method=RequestMethod.GET )
     public ModelAndView deleteComp(@RequestParam("cid") int cid){
        competitionService.delete(cid);
        return new ModelAndView("redirect:DisplayCompetition");
     }
     @RequestMapping(value="/EditCompetition", method=RequestMethod.GET )
      public ModelAndView editCompetition(@RequestParam("cid") int cid){
       Competition c=competitionService.display_by_id(cid);
       return new ModelAndView("EditCompetition","competition",c);
      }
    @RequestMapping(value="/SubmitEditCompetition", method=RequestMethod.POST)
    public ModelAndView submitEditCompetition(@ModelAttribute Competition c){
    competitionService.update(c);
    return new ModelAndView("redirect:DisplayCompetition");
    }
    
    

}