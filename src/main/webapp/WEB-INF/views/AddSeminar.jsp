<%@page buffer="16kb" autoFlush="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    
    <%@include file="HeaderProperties.jsp" %>
    
</head>

<body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        
        <%@include file="TopNavBar.jsp" %>
        
        <!-- main / large navbar -->
        

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Add Seminar</h1>
                            </div>
                        </div>
                    </div>   
                        
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <span>Seminar</span>
                                    </li>
                                    <li class="active">Add Seminar</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Seminar's Form</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                    <form action="SubmitAddSeminar" method="POST" id="validateform" class="form-horizontal">
                                        <fieldset>
                                            <legend>Add Seminar Details</legend>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Seminar Name </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarname" required class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Seminar Date </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminardate" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Seminar Duration </label>
                                                <div class="col-lg-10">
                                                    <input type="text" required name="seminarduration" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Building </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarbuilding" required class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Instructor </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarmaster" required class="form-control" >
                                                </div>
                                            </div>                                            
                                            
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        
                                        </fieldset>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>
        
    <script>
    $(document).ready(function() {
    $('#validateform').formValidation({
        framework: 'bootstrap',
      
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            boarding: {
                validators: {
                    notEmpty: {
                        message: 'The boarding is required and cannot be empty'
                    }
                }
            },
            eventname: {
                validators: {
                    notEmpty: {
                        message: 'The event name is required and cannot be empty'
                    },
                    
                }
            },
            eventtime: {
                validators: {
                    notEmpty: {
                        message: 'The event time is required and cannot be empty'
                    },
                }
            },
            eventduration: {
                validators: {
                    notEmpty: {
                        message: 'The event duration is required and cannot be empty'
                    },
                }
            },
            
        }
    });
});
</script>
        
</body>

</html>
