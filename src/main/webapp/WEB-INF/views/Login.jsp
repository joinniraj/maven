<%-- 
    Document   : login
    Created on : May 7, 2018, 5:15:51 PM
    Author     : Saroj Shrestha
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value=""/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LogIn</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <style type="text/css">
            #box{
                height: 2px;
                width: 1px;
                padding: 5px;
                border-radius: 10px;
                line-height:90px;
            }
            #in{
                width: 90%;
                height: 50px;
                border-radius: 10px;
                padding: 8px;
                border : 4px solid #00cccc;
                background-color: #333333;
                color: white;
            }
            #in:focus{
                border:6px solid #00cccc;
            }
            #inBtn{
                width: 90%;
                height: 50px;
                border-radius: 10px;
                line-height:10px;
                border : 4px solid #00cccc;
                background-color: #333333;
                color:white;
                cursor: pointer;
            }
            #copy{
                color: #00cccc;
                text-align: center;
                font-style: italic;
            }
            footer{
                position:fixed;		    
                bottom:0px;
                left:0px;
                right:0px;
                margin-bottom:0px;
            }
            hr{
                color: #00cccc;
            }
        </style>
    </head>

    <body >

        <div class="container">
            <!--<div class="row">-->
            <div class="col-md-8 col-md-offset-2" style="margin-top: 15%;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 id="blinking">Hello Lets Login</h1>
                    </div>
                    <div class="panel-body">

                        <form action="j_spring_security_check" name="login" method="POST" class="form-signin">
                            <center>
                                <div id="circle">
                                    <div id="box">
                                        <div class="form-group ">
                                            <span></span>
                                            <input id="inputName" name="j_username" type="text" class="form-control" placeholder="Username"
                                                   autofocus="true"/>
                                            <input id="inputPassword" name="j_password" type="password" class="form-control" placeholder="Password"/>
                                            <span></span>

                                            <button class="btn btn-lg btn-primary btn-block" onsubmit="regvalidate()" type="submit">Log In</button>
                                        </div>
                                    </div>
                                </div>
                            </center>

                        </form>

                    </div>

                </div>

            </div>

        </div>
</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></body>
<script>
                                                        $(document).ready(function() {
                                                            $("#circle").hide();
                                                            $("#box").animate({height: "280px"}, "slow");
                                                            $("#box").animate({width: "450px"}, "slow");
                                                            $("#circle").fadeIn(1000);

                                                        });
                                                        function blinker() {
                                                            $('#blinking').fadeOut("slow");
                                                            $('#blinking').fadeIn("slow");
                                                        }
                                                        setInterval(blinker, 1000);
</script>
</html>
